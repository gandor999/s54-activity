let collection = [];

// Write the queue functions below.

function print(){
	return collection;
}

function enqueue(element){


	if(Array.isArray(element)){


		for(let i = 0; i < element.length; ++i){
			collection[collection.length] = element[i];
		}
	}
	else{
		collection[collection.length] = element;
	}

	return collection;
}


function dequeue(){
	let newArray = [];
	
	for(let i = 0; i < collection.length - 1; ++i){
		newArray[i] = collection[i + 1];
	}

	collection = newArray

	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length > 0){
		return false;
	}
	else{
		return true;
	}
}




module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};